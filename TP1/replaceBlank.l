/*
==========================
Replace une suite de blanc par un seul blanc
==========================
*/

/* 
    Section des definitions C ou Lex entre %{ et %}, 
    options et commandes Lex et 
    raccourcis pour les regex entre { et } 
*/

%{
    #include <stdio.h>
%}

/* 
    Section des regles lexicales 
    Liste de couples 
    Regex
    Action en C, pas de space avant !
    L'action en C commence sur la meme ligne entre { et } 
*/

%%

[\ ]+ { printf(" "); }
[\t]+ { printf("\t"); }
[\n]+ { printf("\n"); }

%%

/* Section du code C */

int main()
{
    printf("Hello, Lex\n");
    yyin =  fopen("fichierTest.txt", "r");
    yylex();
    return 0;
}

