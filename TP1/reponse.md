# TP1

## Exercice 1 : Javanais ou Pig Latin

    Le Javanais (en anglais Pig Latin) est un jeu d’enfants où l’on effectue des transformations
    simples sur les voyelles de mots. On vous demande d’écrire un traducteur Français -
    Javanais qui fait précéder chaque groupe de voyelles de av. Ainsi, Prise en main serait
    traduit par Pravisave aven mavain.

coucou -> cavoucavou
Prise en main -> Pravisave aven mavain

### Grammaire CC

Avec une grammaire:

S -> SwS
Sv (voyelle) -> avvD
Sc -> cS
Dv -> vD
Dc (not voyelle) -> cS
DS -> Epsilon
SS -> Epsilon

Application sur ScoucouS
cSoucouS
cavoDucouS
cavouDcouS
cavoucSouS
cavoucavoDuS
cavoucavouDS
cavoucavou

### En python

En python : frToPigLatin.py
$ python frToPigLatin.py "Prise en main"
Pravisave aven mavain

### Avec Lex

Compilation **gcc**: flag **-lfl**
YYLEX Lance l'analyse sur le stream pointé par la var globale **yyin**.
Par défaut, *stdin*. 

Lex compile: 
- `-s`: Supprimme la regle par default qui ECHO le text unmatch
- `-v` : Verbose
- `-p`: Perf report

YYLEX, tente d'analyser les plus longues sequences possibles correspondant à un regex. Regex prioritaire suivant son placement dans la liste. Si match, alors l'action est executée. 
Dans l'action, on peut utiliser:
- **yytext** qui contient la chaine de char reconnue
- **yylong** qui en donne la longueur

Un caractère non reconnu est affiché sur la sortie.

YYLEX se termine quand les fichier est parcouru ou quand une action effectue un return ou quand on appelle **yyterminate()**

CTRL^C: Envoi SIGINT, tue le processus
CTRL^D: Fermeture de stdin, arret naturel

#### Reponse 

`[aieouy]+ { printf("av%s", yytext); }`

## Exercice 2: Comptons les mots !

### 1)

    – Écrire un programme Lex qui copie un fichier en remplaçant chaque suite non vide de
    blancs par un seul blanc.


    (\ )+ { printf(" "); }
    yyin =  fopen("fichierTest.txt", "r");

### 2)

    – Écrire un programme Lex qui compte le nombre de mots d’un texte puis d’un fichier
    dont le nom sera passé en paramètre. Votre programme doit afficher la même valeur
    que la commande Unix wc -w sur le même fichier (/usr/include/stdio.h par exemple).

    $ wc  -w /usr/include/stdio.h
    4202 /usr/include/stdio.h

    $ ./countWords.lex.run /usr/include/stdio.h
    4202 /usr/include/stdio.h

Pour ne rien afficher : `.|\n { /* Action vide */ }`
Faire gaffe avec le printf sur ZSH, le buffer ne se vide pas automatiquement si pas de \n

## Exercice 3: Mon code sur internet

$ ./htmlc.lex.run fichierTest.c > monfichierc.html

## Exercice 4: Table des symboles

Quand on voit un identificateur:
    - Creer une entree dans la liste des symboles

Tables des symboles:
- Tableau (error au dépassement)
- Liste chainnee
