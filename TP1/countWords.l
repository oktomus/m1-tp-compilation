/*
==========================
Compte le nombre de mots dans un fichier donne en premier arg
==========================
*/

%{
    #include <stdio.h>
    size_t words = 0;
%}

/*
[\w\d]+ { ++words; }
[[:alpha:][:lower][:upper:][:alnum:][:digit:][:punct:]]+ { ++words; }
(^[\ \t\n])+ { ++words; }
([^[:blank:]])+ { ++words; }
*/

%%

[^\ \n\t]+ { ++words; }
.|\n { /* Action vide */ }

%%


int main(int argc, const char **argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <fichier>\n", argv[0]);
        exit(1);
    }
    yyin =  fopen(argv[1], "r");
    if (yyin == NULL)
    {
        fprintf(stderr, "Cannot open file %s in read-only\n", argv[1]);
        exit(2);
    }
    yylex();
    fclose(yyin);
    printf("%d %s\n", words, argv[1]);
    // Fflush stdout pour vider le buffer de stdout
    return 0;
}

