/* 
    Section des definitions C ou Lex entre %{ et %}, 
    options et commandes Lex et 
    raccourcis pour les regex entre { et } 
*/

%{
    #include <stdio.h>
%}

/* 
    Section des regles lexicales 
    Liste de couples 
    Regex
    Action en C, pas de space avant !
    L'action en C commence sur la meme ligne entre { et } 
*/

/*
[0-9]+ { printf("~Lex: J'ai vu %s !~", yytext); }
[aieouy]+ { printf("~Lex: J'ai vu une voyelle !~"); }
*/
%%

[aieouy]+ { printf("av%s", yytext); }

%%

/* Section du code C */

int main()
{
    printf("Hello, Lex\n");
    yylex();
    return 0;
}

