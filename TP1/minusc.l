/*
==========================
Analyseur lexicale pour C-
==========================
*/

%{
    #include <stdio.h>
    #include <string.h>
    char ids[1024][128] = {
    "char", "int", "float", "void", "main"};
    size_t available=5;

    int index_of(const char * id)
    {
        size_t i = 0;
        for(i = 0; i < available; ++i)
        {
            if(strcmp(ids[i], id) == 0){
                return i;
            }
        }
        return -1;
    }

    int add_id(const char *id)
    {
        int exists = index_of(id);
        if(exists >= 0) return exists;

        strcpy(ids[available], id);
        return available++;
    }

%}

%%

"+"     { printf(" S=1 "); }
"-"     { printf(" S=2 "); }
"*"     { printf(" S=3 "); }
"/"     { printf(" S=4 "); }
"="     { printf(" S=5 "); }
"if"    { printf(" S=100 "); }
"("     { printf(" S=21 "); }
")"     { printf(" S=22 "); }
"{"     { printf(" S=23 "); }
"}"     { printf(" S=24 "); }
"["     { printf(" S=25 "); }
"]"     { printf(" S=26 "); }
"for"   { printf(" S=112 "); }
"<<"    { printf(" S=45 "); }
">>"    { printf(" S=46 "); }
"||"    { printf(" S=47 "); }
"&&"    { printf(" S=48 "); }
"while" { printf(" S=124 "); }
[0-9]+  { printf(" INT(val=%s) ", yytext); }
([0-9]+"."[0-9]?) { printf(" FLOAT(val=%s) ", yytext); }
[a-zA-Z][a-zA-Z0-9]* { printf(" ID( %d ) ", add_id(yytext)); }
"'"."'" { printf(" CHAR(val=%s) ", yytext); }
" " { printf(""); }
";" { printf(""); }
\t { printf(""); }
. { printf("ERROR, invalid syntax: %s", yytext); }

%%


int main(int argc, const char **argv)
{
    yylex();

    return 0;
}

