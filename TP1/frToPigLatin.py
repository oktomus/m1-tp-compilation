import sys

if __name__ == "__main__":

    word = sys.argv[1]
    newWord = ""
    voy = False;
    subject = ('a', 'e', 'i', 'o', 'u')

    for c in word:

        if c in subject and not voy:
            voy = True
            newWord += "av"
        elif c not in subject:
            voy = False

        newWord += c

    print(newWord)

