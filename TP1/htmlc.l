/*
==========================
Transforme un fichier C en HTML
==========================
*/

%{
    #include <stdio.h>

    const char* gt = "&gt;";
    const char* lt = "&lt;";
%}

%%

(<) { printf("%s", lt); }
(>) { printf("%s", gt); }
"/*"([^*]*[*]+[^*/])*[^*]*[*]+"/" { printf("<span class='comment'>%s</span>", yytext); }
\"([^\"\\]|\\.)*\" { printf("<span class='text'>%s</span>", yytext); }
^"#".*$ { printf("<span class='preproc'>%s</span>", yytext); }
\n { printf("<br>\n"); }
\t { printf("&nbsp;&nbsp;&nbsp;&nbsp;"); }
(\ ) { printf("&nbsp;"); }
(asm|auto|break|catch|case|char|class|const|continue|default|delete|do|double|else|enum|extern|float|for|friend|goto|if|inline|int|long|new|operator|overload|private|protected|public|register|return|short|signed|sizeof|static|struct|switch|this|template|typedef|union|unsigned|virtual|void|volatile|while) { printf("<span class='kw'>%s</span>", yytext); }
. { printf("%s", yytext); }

%%


int main(int argc, const char **argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <fichier>\n", argv[0]);
        exit(1);
    }
    yyin =  fopen(argv[1], "r");
    if (yyin == NULL)
    {
        fprintf(stderr, "Cannot open file %s in read-only\n", argv[1]);
        exit(2);
    }
    printf("<html>\n"
    "<head>\n<style>\n"
    "body {background-color: darkgrey;}\n"
    ".kw { font-weigth: bold; color:orange;}\n"
    ".preproc { color: grey;}\n"
    ".comment { color: red;}\n"
    ".text { color: green;}\n"
    "</style></head><body>\n");
    yylex();
    fclose(yyin);
    printf("</body></html>\n");
    // Fflush stdout pour vider le buffer de stdout
    return 0;
}

