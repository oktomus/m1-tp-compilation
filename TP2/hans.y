%{

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    int yylex();
    void yyerror(void *);

    typedef struct ensemble{
        char* name;
        //int [10] vals;
        int size;
    } ensemble;

    typedef struct ensemble_link{
        ensemble * current;
        struct ensemble_link * next;
    } ensemble_link;

    void new_ensemble(char*);
    void ensemble_print(char*);
    void ensemble_add(ensemble*, int);

    ensemble_link * list = 0;
    ensemble_link * last_link = 0;

%}

%union {
    char* name;
    int value;
}

%token <value> ENTIER
%token EMPTY
%token ASSIGN

%token <name> IDENT

%start liste

%%

liste: '\n' { printf("See ya!\n"); return 0; }
     | liste instruction '\n' { printf("Liste instruction\n"); }

instruction : IDENT ASSIGN expression { printf("Assignation vers %s\n", $1); }
            | IDENT {ensemble_print($1);}

expression : operande

operande : ensemble

ensemble : '{' '}' { printf("Ensemble vide\n"); }

%%

void new_ensemble(char* name)
{
    if(list == 0)
    {
        list = malloc(sizeof(ensemble_link));
        last_link = list;
    }
    else
    {
        last_link->next = malloc(sizeof(ensemble_link));
        last_link = last_link->next;
    }

    ensemble * new = malloc(sizeof(ensemble));
    new->name = strdup(name);
    new->size = 0;
    last_link->current = new;
}

void ensemble_print(char* name)
{
    ensemble_link * hans = list;

    while(strcmp(name, hans->current->name) != 0)
        hans = hans->next;

    ensemble * printer = hans->current;

    int i = 0;
    printf("%s {", printer->name);
    for(i = 0; i < printer->size; ++i)
    {
        //printf("%d", hans->vals[i]);
        if(i < printer->size - 1)
            printf(", ");
    }
    printf("}\n");
}

void ensemble_add(ensemble* hans, int val)
{

}

int main(){

    printf("Hans-sembliste\n");
    fflush(stdout);

    return yyparse();

}
