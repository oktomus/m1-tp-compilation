%{

    #include <stdio.h>
    #include <stdlib.h>
    int yylex();
    void yyerror(void *);

%}

%token ENTIER
%right '='
%left '+' '-'
%left '*'
%start ligne

%%

ligne: operation { printf("Résultat: %d\n", $1); fflush(stdout); }

operation: operation '+' operation { $$ = $1 + $3; }

         | '-' operation { $$ = -$2; }

         | operation '*' operation { $$ = $1 * $3; }

         | '(' operation ')' { $$ = $2;}

         | ENTIER { $$ = $1;}

         ;

%%


int main(){

    printf("Entrez une operation: \n");
    fflush(stdout);

    return yyparse();

}
