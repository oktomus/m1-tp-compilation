%{
    #include <stdio.h>
    #include <string.h>
    #include "y.hans.h"
%}

%%

[0-9]+ {yylval.value = atoi(yytext); return ENTIER;}
[a-zA-Z]+ { yylval.name = strdup(yytext); return IDENT; }
":=" { return ASSIGN; }
[{] { return '{'; }
[}] { return '}'; }
[\n] { return yytext[0]; }
" " { return ' ';}
. { printf("Element inconnu: %s\n", yytext); return yytext[0];}

%%

