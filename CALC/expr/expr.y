%{
  #include <stdio.h>
  #include <stdlib.h>

  void yyerror(char*);
  int yylex();

  typedef struct ast{
    int type;
    %union{
        struct operation{
            ast * left;
            ast * right;
            int type;
        }
  } ast;

%}

%union {
    int value;
    char * string;
}

%token <string> IDENTIFIER
%token <value> NUMBER
%type <value> expression
%type <value> axiom

%left '+'
%left '*'

%%

axiom:
    expression '\n'
    { 
      printf("Match :%d) !\n", $$);
      return 0;
    }
  ;

expression:
    expression '+' expression
    { 
      printf("expression -> expression + expression\n");
      $$ = $1 + $3;
    }
  | expression '*' expression
    {
      printf("expression -> expression * expression\n");
      $$ = $1 * $3;
    }
  | '-' expression 
    {
      printf("expression -> - expression\n");
      $$ = - $2;
    }
  | '(' expression ')'
    {
      printf("expression -> ( expression )\n");
      $$ = $2;
    }
  | IDENTIFIER
    {
      printf("expression -> IDENTIFIER (%s)\n", $1);
      $$ = 0;
    }
  | NUMBER
    {
      printf("expression -> NUMBER (%d)\n", $1);
      $$ = $1;
    }
  ;

%%

void yyerror (char *s) {
    fprintf(stderr, "[Yacc] error: %s\n", s);
}

int main() {
  printf("Enter an arithmetic expression:\n");
  yyparse();
  printf("-----------------\nSymbol table:\n");
  printf("-----------------\nQuad list:\n");

  return 0;
}
